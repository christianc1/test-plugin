=== Exanoke ===
Contributors:      Christian Chung
Donate link:       http://github.com/christianc1
Tags: 
Requires at least: 4.1.1
Tested up to:      4.1.1
Stable tag:        0.1.0
License:           GPLv2 or later
License URI:       http://www.gnu.org/licenses/gpl-2.0.html

An example plugin

== Description ==



== Installation ==

= Manual Installation =

1. Upload the entire `/basis-test-plugin` directory to the `/wp-content/plugins/` directory.
2. Activate Example through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 0.1.0 =
* First release

== Upgrade Notice ==

= 0.1.0 =
First Release
