module.exports = {
	dist: {
		options: {
			processors: [
				require('autoprefixer')({browsers: 'last 2 versions'})
			]
		},
		files: { 
			'assets/css/sportlabs-content-importer.css': [ 'assets/css/sportlabs-content-importer.css' ]
		}
	}
};