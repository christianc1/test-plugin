module.exports = {
	all: {
		options: {
			precision: 2,
			sourceMap: true
		},
		files: {
			'assets/css/sportlabs-content-importer.css': 'assets/css/sass/sportlabs-content-importer.scss'
		}
	}
};