module.exports = {
	main: {
		options: {
			mode: 'zip',
			archive: './release/slci.<%= pkg.version %>.zip'
		},
		expand: true,
		cwd: 'release/<%= pkg.version %>/',
		src: ['**/*'],
		dest: 'slci/'
	}
};