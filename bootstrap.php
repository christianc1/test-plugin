<?php
/**
 * Plugin Name
 *
 * @package     Example Plugin
 * @author      Your Name
 * @copyright   2016 Your Name or Company Name
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Example Plugin
 * Plugin URI:  https://bitbucket.org/christianc1/test-plugin/src
 * Description: Example of WordPress plugin with PSR-4 and Composer support
 * Version:     0.1.0
 * Author:      Christian Chung
 * Author URI:  https://christianchung.com
 * Text Domain: example-plugin
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

if( function_exists( 'add_action' ) ) {
	add_action( 'sc_ci_init', function() {
		Lift\Examples\Core\setup(); 
	});
}